What is done 

## Pure js
Basic functionality:
- Autocomplete Input
- Container for results
- When you type `n` letters you'll need to wait `m` ms before you call the search
- Ability to provide source callback, which will return to you an array of JSON objects
- Ability to provide several source callbacks and use them one by one until the result of callback return not empty array. 
- Add ability to pass a render callback. this callback should receive the result from source
- Add ability to pass a render callbacks, which should be chosen depending on the source callback, which would return not empty results 
- Add ability to pass custom css for input/container/etc
- Loading Spinner
- Minifying with webpack 

## React
## For testing react app you should change branch from "master" to "test-task-react"
There will be 2 blocks, which should be in a row if possible or in a column if not (simple responsiveness).
- First block - integrate autocomplete and wrap it in a ReactJS Component, which will give an ability to search city.
When you press the city, then the second block appears and should be in focus.
- Second block - is a section with Chart of amounts of data (population/cost of living, etc)/time.
For charts you can use any library you want.


